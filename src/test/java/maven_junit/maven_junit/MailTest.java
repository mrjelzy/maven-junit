package maven_junit.maven_junit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runners.Parameterized;

import courriel.mail;
import courriel.EnvoiInvalide;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

public class MailTest {
	
	/*
	 * mail mailtest = new mail("le mail de test", "zdbqhzebfez@hotmail.fr" ,
	 * "le corps du mail"); mail mailtest2 = new mail("le mail de test2",
	 * "ryan@hotmail.fr" , "le corps du mail");
	 */
	
	
	
	/*
	 * @Test public void testDestinataireValide() {
	 * assertTrue(mailtest.destinataireValide());
	 * assertTrue(mailtest2.destinataireValide()); }
	 */
	
	@ParameterizedTest
    @ValueSource(strings = { "zdbqhzebfez@hotmail.fr","ryan@hotmail.fr", "zdbqhzebfez@oui.fr" })
	public void testDestinataireParams(String adress) {
		mail mailtest3 = new mail("le mail de test2", adress , "le corps du mail");
		assertTrue(mailtest3.destinataireValide());
	}
	
	@ParameterizedTest(name = "#{index} - Adress Mail {0}? {1}")
	@CsvSource({
		 "zdbqhzebfez@hotmail.fr, carractere numerique a la fin",
		 "zdbqhzebfez@hotmail.fr ,   OK",
		 "zdbqhzebfez@hotmail.fr,    carractere numerique au debut"
	 })
	public void testDestinataireParams2(String adress, String response) {
		mail mailtest3 = new mail("le mail de test2", adress , "le corps du mail");
		assertTrue(mailtest3.destinataireValide());
	}
		
    @ParameterizedTest(name = "run with adress : {0}, problème : {1}")
    @MethodSource("badAdresses")
    public void testAdresses(String adress, String problem) {
    	mail mailtest3 = new mail("le mail de test2", adress , "le corps du mail");
        EnvoiInvalide thrown = assertThrows(EnvoiInvalide.class, () -> {mailtest3.envoyer();});
        assertTrue(thrown.getMessage().equals("Adresse du destinataire incorrecte"));
    }
    
    public static Stream<Arguments> badAdresses() {
        return Stream.of(
            Arguments.of("zdbqhzebfezhotmail.fr", "Pas d'arobase"),
            Arguments.of("@eee.fr", "Rien avant l'arobase"),
            Arguments.of("aaa@aaaa.2", "Nom de domaine avec chiffre")
        );
    }
}

