package courriel;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {
	    
	public static void main(String[] args) {
		
	    ObjectMapper mapper = new ObjectMapper();
	    try {
	    	
			List<Groupe> json = Arrays.asList(mapper.readValue(Paths.get("groupes.json").toFile(), Groupe[].class));
		    json.forEach(System.out::println);
		    
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}