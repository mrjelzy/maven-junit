package courriel;

public class Groupe {
	private String idGroupe;
	private String nomGroupe;
	private String idSujet;
	
	public Groupe(){
		super();
	}
	
	public Groupe(String idGroupe, String nomGroupe, String idSujet) {
		setIdGroupe(idGroupe);
		setnomGroupe(nomGroupe);
		setIdSujet(idSujet);
	}
	
	public String getIdGroupe() {
		return idGroupe;
	}
	public void setIdGroupe(String idGroupe) {
		this.idGroupe = idGroupe;
	}
	public String getnomGroupe() {
		return nomGroupe;
	}
	public void setnomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}
	public String getIdSujet() {
		return idSujet;
	}
	public void setIdSujet(String idSujet) {
		this.idSujet = idSujet;
	}

	@Override
	public String toString() {
		return "groupe [idGroupe=" + idGroupe + ", nomGroupe=" + nomGroupe + ", idSujet=" + idSujet + "]";
	}
	
	
}
