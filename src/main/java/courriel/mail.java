package courriel;

import java.util.ArrayList;

public class mail {
	private String titre;
	private String destinataire;
	private String corps;
	private ArrayList<String> piecesjointes = new ArrayList<String>();
	
	public mail(String titre, String destinataire, String corps) {
		this.titre = titre;
		this.destinataire = destinataire;
		this.corps = corps;
	}
	
	public String getTitre() {
		return titre;
	}

	public String getDestinataire() {
		return destinataire;
	}

	public String getCorps() {
		return corps;
	}

	public ArrayList<String> getPiecesjointes() {
		ArrayList<String> resultat=new ArrayList<String>();
		for (String c: this.piecesjointes){
			resultat.add(c);
		}
		return resultat;
	}
	
	public void ajoutPJ(String chemin) {
		this.piecesjointes.add(chemin);
	}
	
	public boolean destinataireValide() {
		return this.destinataire.matches("^[a-z][a-z0-9]*@[a-z]+[.][a-z]*");
	}
	public void envoyer() throws EnvoiInvalide {
        //verifier présence adresse
        if(!this.destinatairePresente()) {
            throw new EnvoiInvalide("Pas de destinataire");
        }
        //verifier adresse bien formée
        if(!this.destinataireValide()) {
            throw new EnvoiInvalide("Adresse du destinataire incorrecte");
        }
        //verifier présence titre
        if(!this.titrePresent()) {
            throw new EnvoiInvalide("Pas de titre");
        }
        //verifier si message contient "joint", si oui si présence d'au moins une pièce jointe
        if(this.motCleJoin() && !this.pieceJointePresente()) {
            throw new EnvoiInvalide("Pas de piece jointe");
        }
    }

    private boolean destinatairePresente() {
        return !(destinataire.length()==0);
    }
    
    private boolean titrePresent() {
        return !(titre.length()==0);
    }

    private boolean motCleJoin() {
        return corps.matches("(.*)(joint|jointe|PJ)(.*)");
    }

    private boolean pieceJointePresente() {
        return !(piecesjointes.size() == 0);
    }
}
